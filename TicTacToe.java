import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {
	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in);
		char[][] board = { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };
		int meter = 0;
		//boolean tictac = true;
		printBoard(board);




		while (true){
			System.out.print("Player 1 enter row number:");
			int row = reader.nextInt();
			System.out.print("Player 1 enter column number:");
			int col = reader.nextInt();

			while ( (row < 1 || row > 3) || (col < 1 || col > 3 ) || (board[row-1][col-1] != ' ')) {
				System.out.println("This space is occupied OR coordinates out of range");
				System.out.print("Player 1 enter row number:");
				row = reader.nextInt();
				System.out.print("Player 1 enter column number:");
				col = reader.nextInt();
			}
			board[row - 1][col - 1] = 'X';
			printBoard(board);
			meter += 1;
			if (checkBoard(board)) {
				//tictac == false;
				System.out.println("Player 1 is winner.");
				break;
			}
			if (meter >= 9) {
				System.out.println("NO WINNER");
				//tictac == false;
				break;
			}



			System.out.print("Player 2 enter row number:");
			row = reader.nextInt();
			System.out.print("Player 2 enter column number:");
			col = reader.nextInt();
			while ( (row < 1 || row > 3) || (col < 1 || col > 3 ) || (board[row-1][col-1] != ' ')) {
				System.out.println("This space is occupied OR coordinates out of range");
				System.out.print("Player 2 enter row number:");
				row = reader.nextInt();
				System.out.print("Player 2 enter column number:");
				col = reader.nextInt();
			}
			board[row - 1][col - 1] = 'O';
			printBoard(board);
			meter += 1;
			if (checkBoard(board)) {
				//tictac == false;
				System.out.println("Player 2 is winner.");
				break;
			}
		}
		reader.close();
	}


	public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");
			}
			System.out.println();
			System.out.println("   -----------");
		}
	}




	public static boolean checkBoard(char[][] board) {
		// for horizontal and vertical
		for (int j=0; j<3; ++j) {
			if (board[0][j] != ' ' && board[1][j] == board[0][j] && board[2][j] == board[0][j])
				return true;
			else if (board[j][0] != ' ' && board[j][0] == board[j][2] && board[j][0] == board[j][1])
				return true;
		// for crossovers
		}if ( board[0][2] != ' ' && board[0][2] == board[2][0] && board[0][2] == board[1][1]) {
			return true;
		}else return board[0][0] != ' ' && board[0][0] == board[1][1] && board[0][0] == board[2][2];
	}
}